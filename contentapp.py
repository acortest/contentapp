import webapp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

class ContentApp(webapp.webApp): #heredo el contenido de webapp aqui, que es el init por que el resto no hace nada, es decir me olvido de los sockets

    contents = {'/': "<p>Welcome to the main page</p>",
                '/hello': "<p>Hello, people</p>",
                '/bye': "<p>Bye all!!</p>",
                '/pepito': "<p> Esta es la pagina de pepito </p>"}

    def parse (self, request):  #SELF INDICA QUE USAMOS LA VARIABLE QUE ESTA DENTRO DEL CONTENT, EN ESTE CASO CONTENTAPP
        """Return the resource name"""

        return request.split(' ',2)[1]

    def process (self, resource):
        """Produce the page with the content for the resource"""

        if resource in self.contents:
            content = self.contents[resource]
            page = PAGE.format(content=content)
            code = "200 OK"
        else:
            page = PAGE_NOT_FOUND.format(resource=resource)
            code = "404 Resource Not Found"
        return (code, page)

if __name__ == "__main__":
    webApp = ContentApp ("localhost", 1235)
